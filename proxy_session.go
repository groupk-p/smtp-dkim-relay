package main

import (
	"io"

	"github.com/emersion/go-smtp"
)

type ProxySession struct {
	c  *smtp.Client
	be *ProxyBackend
}

func (s *ProxySession) Reset() {
	s.c.Reset()
}

func (s *ProxySession) Mail(from string, opts smtp.MailOptions) error {
	return s.c.Mail(from, &opts)
}

func (s *ProxySession) Rcpt(to string) error {
	return s.c.Rcpt(to)
}

func (s *ProxySession) Data(r io.Reader) error {
	wc, err := s.c.Data()
	if err != nil {
		return err
	}

	_, err = io.Copy(wc, r)
	if err != nil {
		wc.Close()
		return err
	}

	return wc.Close()
}

func (s *ProxySession) Logout() error {
	return s.c.Quit()
}
