/*
	smtp-dkim-signer - SMTP-proxy that DKIM-signs e-mails before submission.
	Copyright (C) 2018 - 2020, Marc Hoersken <info@marc-hoersken.de>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"crypto"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"strings"

	dkim "github.com/emersion/go-msgauth/dkim"
)

func loadPrivKey(privkeypath string) (*rsa.PrivateKey, error) {
	var block *pem.Block
	privkey := strings.TrimSpace(privkeypath)
	if strings.HasPrefix(privkey, "-----") &&
		strings.HasSuffix(privkey, "-----") {
		block, _ = pem.Decode([]byte(privkey))
	} else {
		splits := strings.Split(privkeypath, "\n")
		filepath := strings.TrimSpace(splits[0])
		bytes, err := ioutil.ReadFile(filepath)
		if err != nil {
			return nil, err
		}
		block, _ = pem.Decode(bytes)
	}
	key, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return key, err
}

func makeOptions(cfg *config, cfgvh *configVHost) (*dkim.SignOptions, error) {
	if cfg == nil || cfgvh == nil {
		return nil, fmt.Errorf("this should never happen")
	}
	var Domain string
	var Selector string = cfg.DefaultHost.Selector
	var PrivKeyPath string = cfg.DefaultHost.PrivKeyPath
	var HeaderCan string = cfg.DefaultHost.HeaderCan
	var BodyCan string = cfg.DefaultHost.BodyCan
	var HeaderKeys []string

	if cfgvh.Domain != "" {
		Domain = cfgvh.Domain
	}
	if cfgvh.Selector != "" {
		Selector = cfgvh.Selector
	}
	if cfgvh.PrivKeyPath != "" {
		PrivKeyPath = cfgvh.PrivKeyPath
	}
	if cfgvh.HeaderCan != "" {
		HeaderCan = cfgvh.HeaderCan
	}
	if cfgvh.BodyCan != "" {
		BodyCan = cfgvh.BodyCan
	}
	if len(cfgvh.HeaderKeys) != 0 {
		HeaderKeys = cfgvh.HeaderKeys
	} else {
		cfgvh.HeaderKeys = cfg.HeaderKeys
	}

	if Domain == "" {
		return nil, fmt.Errorf("no VirtualHost.Domain specified")
	}
	if Selector == "" {
		return nil, fmt.Errorf("no VirtualHost.Selector specified")
	}
	if PrivKeyPath == "" {
		return nil, fmt.Errorf("no VirtualHost.PrivKeyPath specified")
	}

	privkey, err := loadPrivKey(PrivKeyPath)
	if err != nil {
		return nil, fmt.Errorf("unable to load VirtualHost.PrivKeyPath due to: %s", err)
	}

	dkimopt := &dkim.SignOptions{
		Domain:                 Domain,
		Selector:               Selector,
		Signer:                 privkey,
		Hash:                   crypto.SHA256,
		HeaderCanonicalization: dkim.Canonicalization(HeaderCan),
		BodyCanonicalization:   dkim.Canonicalization(BodyCan),
		HeaderKeys:             HeaderKeys,
	}
	return dkimopt, nil
}
