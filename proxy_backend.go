package main

import (
	"crypto/tls"
	"errors"
	"net"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)

type Security int
type LoginMethod int

const (
	SecurityTLS Security = iota
	SecurityStartTLS
	SecurityNone
)

const (
	LoginClient LoginMethod = iota
	PlainClient
)

type ProxyBackend struct {
	Addr      string
	Security  Security
	TLSConfig *tls.Config
	LMTP      bool
	Host      string

	unexported struct{}
}

func New(addr string) *ProxyBackend {
	return &ProxyBackend{Addr: addr, Security: SecurityStartTLS}
}

func NewTLS(addr string, tlsConfig *tls.Config) *ProxyBackend {
	return &ProxyBackend{
		Addr:      addr,
		Security:  SecurityTLS,
		TLSConfig: tlsConfig,
	}
}

func NewLMTP(addr string, host string) *ProxyBackend {
	return &ProxyBackend{
		Addr:     addr,
		Security: SecurityNone,
		LMTP:     true,
		Host:     host,
	}
}

func (be *ProxyBackend) newConn() (*smtp.Client, error) {
	var conn net.Conn
	var err error
	if be.LMTP {
		if be.Security != SecurityNone {
			return nil, errors.New("smtp-proxy: LMTP doesn't support TLS")
		}
		conn, err = net.Dial("unix", be.Addr)
	} else if be.Security == SecurityTLS {
		conn, err = tls.Dial("tcp", be.Addr, be.TLSConfig)
	} else {
		conn, err = net.Dial("tcp", be.Addr)
	}
	if err != nil {
		return nil, err
	}

	var c *smtp.Client
	if be.LMTP {
		c, err = smtp.NewClientLMTP(conn, be.Host)
	} else {
		host := be.Host
		if host == "" {
			host, _, _ = net.SplitHostPort(be.Addr)
		}
		c, err = smtp.NewClient(conn, host)
	}
	if err != nil {
		return nil, err
	}

	if be.Security == SecurityStartTLS {
		if err := c.StartTLS(be.TLSConfig); err != nil {
			return nil, err
		}
	}

	return c, nil
}

func (be *ProxyBackend) login(username, password string, clientAuthMethod LoginMethod) (*smtp.Client, error) {
	c, err := be.newConn()
	if err != nil {
		return nil, err
	}

	var auth sasl.Client
	if clientAuthMethod == LoginClient {
		auth = sasl.NewLoginClient(username, password)
	} else {
		auth = sasl.NewPlainClient("", username, password)
	}

	if err := c.Auth(auth); err != nil {
		return nil, err
	}

	return c, nil
}

func (be *ProxyBackend) Login(state *smtp.ConnectionState, username, password string, clientAuthMethod LoginMethod) (smtp.Session, error) {
	c, err := be.login(username, password, clientAuthMethod)
	if err != nil {
		return nil, err
	}

	s := &ProxySession{
		c:  c,
		be: be,
	}
	return s, nil
}

func (be *ProxyBackend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	c, err := be.newConn()
	if err != nil {
		return nil, err
	}

	s := &ProxySession{
		c:  c,
		be: be,
	}
	return s, nil
}
